#ifndef FOOD_THINKER_HPP
# define FOOD_THINKER_HPP

# include <fstream>
# include <iostream>
# include <math.h>
# include <stdexcept>
# include <string>
# include <string.h>
# include <time.h>
# include <vector>

# define MINUTES_PER_DAY	24 * 60
# define DAY_TICK			1. / MINUTES_PER_DAY

namespace FoodThinker
{
	using namespace std;

	string read_file(const string file);

	struct Neuron
	{
		vector<float> inputs;

		inline Neuron(const size_t inputs)
		{
			this->inputs.reserve(inputs);
		}

		float activate(const vector<float>& inputs);
		void adjust(const float delta);
	};

	struct Layer
	{
		vector<Neuron> neurons;

		Layer(const size_t neurons);

		void resize(const size_t neurons);

		vector<float> activate(const vector<float>& inputs);
		void adjust(const float delta);
	};

	class Network
	{
		public:
			Network(const string file);

			void set_inputs(const size_t inputs);
			void set_outputs(const size_t outputs);
			void set_layers(const size_t layers);
			void set_neurons_per_layer(const size_t neurons);

			inline const vector<Layer>& get_layers() const
			{
				return layers;
			}

			vector<float> activate(vector<float> inputs);
			void adjust(const float delta);

			void save() const;

		private:
			const string file;

			size_t inputs;
			size_t outputs;

			vector<Layer> layers;
			size_t neurons_per_layer;
	};

	struct Meal
	{
		float value;
		string name;
		// TODO Add nutritive informations

		string dump() const;
	};

	vector<Meal> load_meals(const string file);
	Meal guess(const vector<Meal>& meals, Network& network);
}

#endif
