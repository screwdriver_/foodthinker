#include "food_thinker.hpp"

using namespace FoodThinker;

static inline float sigmoid(const float x)
{
	return 1. / (1. + exp(-x));
}

float Neuron::activate(const vector<float>& inputs)
{
	if(inputs.size() != this->inputs.size()) {
		throw invalid_argument("Invalid input values!");
	}

	float output = 0;

	for(size_t i = 0; i < inputs.size(); ++i) {
		output += inputs[i] * this->inputs[i];
	}

	return sigmoid(output);
}

void Neuron::adjust(const float delta)
{
	for (size_t i = 0; i < inputs.size(); ++i) {
		inputs[i] *= delta;
	}
}
