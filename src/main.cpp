#include "food_thinker.hpp"

using namespace std;

static uint8_t grade()
{
	char c;

	do {
		cout << "Grade this guess (from 0 to 9):" << '\n';
		cin >> c;
	} while(!(c >= '0' && c <= '9'));

	return (c - '0');
}

int main()
{
	using namespace FoodThinker;

	cout << "Welcome into FoodThinker!" << '\n';
	cout << "Loading data..." << '\n';

	Network network("brain.data");

	cout << "Loading meals list..." << '\n';

	const auto meals = load_meals("meals.data");

	if(meals.empty()) {
		cout << "Empty meals list, exiting..." << '\n';
		exit(-1);
	}

	cout << "Ready!" << '\n';
	cout << "Guessing which meal you could be willing to eat now..." << '\n';

	const auto meal = guess(meals, network);
	cout << '\n';
	cout << "Maybe, " << meal.dump() << '?' << '\n';

	cout << "Ajusting recommendations..." << '\n';
	const auto g = grade();
	network.adjust(0.5 + (1. / g));

	cout << "Saving..." << '\n';
	network.save();
}
