#include "food_thinker.hpp"

using namespace FoodThinker;

string Meal::dump() const
{
	return name;
}

vector<Meal> FoodThinker::load_meals(const string file)
{
	vector<Meal> meals;
	string buffer;

	try {
		buffer = read_file(file);
	} catch(const exception&) {
		return meals;
	}

	for(size_t i = 0; i < buffer.size();) {
		Meal meal;

		memcpy(&meal.value, buffer.data() + i, 4);
		i += 4;

		while(i < buffer.size()) {
			const auto c = buffer[i];
			if(c == '\0') break;

			meal.name += buffer[i];
			++i;
		}

		++i;

		// TODO Nutritive informations

		meals.push_back(meal);
	}

	return meals;
}

static const Meal& get_closer_meal(const vector<Meal>& meals, const float result)
{
	size_t closer;
	float closer_value;

	for(size_t i = 0; i < meals.size(); ++i) {
		const auto& meal = meals[i];
		if(meal.value == result) return meal;

		if(i == 0 || abs(result - meal.value) < abs(result - closer_value)) {
			closer = i;
			closer_value = meal.value;
		}
	}

	return meals[closer];
}

Meal FoodThinker::guess(const vector<Meal>& meals, Network& network)
{
	// TODO Determine the amount of layers and neurons per layer needed
	network.set_inputs(2);
	network.set_outputs(1);

	const auto current_time = time(0);
	const auto t = localtime(&current_time);

	const float day_ticks = (t->tm_hour * 60) + t->tm_min * DAY_TICK;
	const float week_ticks = t->tm_wday / 6.;

	vector<float> inputs;
	inputs.resize(2);
	inputs[0] = day_ticks;
	inputs[1] = week_ticks;

	const auto result = network.activate(inputs).front();
	return get_closer_meal(meals, result);
}
